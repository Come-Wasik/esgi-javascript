function type_check_v1(variable, type) {
    switch (typeof variable) {
        case "symbol":
        case "number":
        case "string":
        case "boolean":
        case "undefined":
        case "function":
            return type === typeof variable;
        case "object":
            switch (type) {
            case "null":
                return variable === null;
            case "array":
                return Array.isArray(variable);
            default:
                return variable !== null && !Array.isArray(variable);
            }
    }
}


function type_check_v2(variable, conf) {
    for (toCheck in conf) {
        switch (toCheck) {
            case "type":
            if (type_check_v1(variable, conf.type) === false) return false;
            break;
            case "value":
            if (JSON.stringify(variable) !== JSON.stringify(conf.value))
                return false;
            break;
            case "enum":
            let found = false;
            for (let i = 0; i < conf.enum.length && found === false; i++) {
                const subValue = conf.enum[i];
                if (type_check_v2(variable, { value: subValue })) {
                    found = true;
                }
            }
            if (!found) {
                return false;
            };
            break;
        }
    }
    return true;
}

function type_check_v3(variable, conf) {

}

console.log(type_check_v2(2, {enum: [1, 2, 3]}));